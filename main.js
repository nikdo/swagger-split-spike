var _ = require('underscore')
var fs = require('fs')

function readSpec(path) {
  return new Promise((resolve, reject) => {
    fs.readFile(path, (err, data) => {
      if (err)
        reject(err)
      else
        resolve(JSON.parse(data))
    })
  })
}

function toTitleCase(str)
{
    return str
      .replace('-', ' ')
      .replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase()})
}

function getRefs(obj) {
  const key = '$ref'
  if (_.has(obj, key))
      return obj[key];
  return _.flatten(_.map(obj, function(v) {
      return typeof v == "object" ? getRefs(v, key) : [];
  }), true);
}

function exportGroupSpec(spec, tag) {
  // assumit that:
  // - all defined tags are used
  // - path operations have the same tag
  // (missing validations)

  var result = _.clone(spec)
  result.tags = _.filter(spec.tags, (tagDefinition) => tagDefinition.name == tag)
  result.paths = _.pick(spec.paths, (value, key, object) => _.every(value, (operation) => operation.tags[0] == tag))
  result.info.title = toTitleCase(result.tags[0].name)
  result.info['x-group-id'] = result.tags[0].name
  result.info.description = result.tags[0].description


  var pathReferencedDefinitions = _.uniq(getRefs(result.paths))
  result.definitions = _.pick(spec.definitions, (definition, name) => pathReferencedDefinitions.indexOf('#/definitions/' + name) >= 0)

  // definitions referenced in defintions missing

  const filename = 'output/' + tag + '.json'
  fs.writeFileSync(filename, JSON.stringify(result, null, 8))
  console.log('➡️  ' + filename)
}

readSpec('data-warehouse.json')
  .then((spec) => {

    console.log('Every operation has a single valid tag: ', _.every(spec.paths, (path) =>
      _.every(path, (operation) => {
        return operation.tags.length == 1 && _.indexOf(spec.tags, operation.tags[0])
      })
    ))

    spec.tags.forEach((tag) => {
      exportGroupSpec(spec, tag.name)
    })



  })
